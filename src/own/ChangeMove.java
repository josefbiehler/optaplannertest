package own;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.optaplanner.core.impl.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;

public class ChangeMove implements Move {
	
	private Computer toComputer;
	private Process process;
	
	public ChangeMove(Computer c, Process p) {
		toComputer = c;
		process = p;
	}

	@Override
	public Move createUndoMove(ScoreDirector arg0) {
		return new ChangeMove(process.getComputer(), process);
	}

	@Override
	public void doMove(ScoreDirector scoreDirector) {
		// TODO Auto-generated method stub
		scoreDirector.beforeVariableChanged(process, "computer");
		process.setComputer(toComputer);
		scoreDirector.afterVariableChanged(process, "computer");
		
	}

	@Override
	public Collection<? extends Object> getPlanningEntities() {
		return Collections.singletonList(process);
	}

	@Override
	public Collection<? extends Object> getPlanningValues() {
		return Collections.singletonList(toComputer);
	}

	@Override
	public boolean isMoveDoable(ScoreDirector arg0) {
		if (toComputer.equals(process.getComputer())) 
				return false;
		// if there 
		CPSolution cp = (CPSolution)arg0.getWorkingSolution();
		/*for(Computer c : cp.getComputer()) {
			if (!c.equals(toComputer) && getUsed(c, cp) < process.ram) {
				return false;
			}
		}*/
		return true;
	}
	
	private int getUsed(Computer c, CPSolution cp) {
		int used = 0;
		for(Process p : cp.getProcesses()) {
			if (used >= c.ram) {
				break;
			}
			if (used < c.ram && c.equals(p.getComputer())) {
				used = used + p.ram;
			}
		}
		return used;
	}
	
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof ChangeMove) {
            ChangeMove other = (ChangeMove) o;
            return process.equals(other.process) && toComputer.equals(other.toComputer);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(process)
                .append(toComputer)
                .toHashCode();
    }

}
