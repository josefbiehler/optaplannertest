package own;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.move.Move;

public class ChangeMoveFactory implements MoveListFactory<CPSolution>{

	@Override
	public List<? extends Move> createMoveList(CPSolution arg0) {
		List<Move> moves = new ArrayList<>();
		for(Process p : arg0.getProcesses()) {
			for(Computer c : arg0.getComputer()) {
				moves.add(new ChangeMove(c, p));
			}
		}
		return moves;
	}

}
