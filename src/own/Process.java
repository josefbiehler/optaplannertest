package own;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class Process extends Entity{
	private Computer computer;
	public int ram;
	
	@PlanningVariable(valueRangeProviderRefs = "computerList")
	public Computer getComputer() {
		return computer;
	}
	
	public void setComputer(Computer c) {
		computer = c;
	}
	
	public static int id;
	public int ownId;
	public void incId() {
		ownId = id;
		id++;
	}
	public int getId() {
		return ownId;
	}
}
