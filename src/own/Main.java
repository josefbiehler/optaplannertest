package own;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.XmlSolverFactory;

import persistence.CloudBalancingGenerator;
import domain.CloudBalance;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        // Build the Solver
        SolverFactory solverFactory = new XmlSolverFactory(
                "/schwuchtel/config.xml");
        Solver solver = solverFactory.buildSolver();

        // Load a problem with 400 computers and 1200 processes
        CPSolution unsolved = new CPSolution(1000, 99);
        
        // Solve the problem
        solver.setPlanningProblem(unsolved);
        solver.solve();
        CPSolution solved = (CPSolution) solver.getBestSolution();

        System.out.println(solved.toString());
        // Display the result
        /*System.out.println("\nSolved cloudBalance with 400 computers and 1200 processes:\n"
                + toDisplayString(solvedCloudBalance));*/
	}

}
