package own;

import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.simple.SimpleScoreCalculator;

public class SimpleCalculator implements SimpleScoreCalculator<CPSolution> {

	@Override
	public Score calculateScore(CPSolution arg0) {
		int hard = 0, soft = 0, ccount = 0, pcount = 0;
		boolean used;
		for(Computer computer : arg0.getComputer()) {
			int ram = 0;
			ccount++;
			used = false;
			for(Process process : arg0.getProcesses()) {
				if (computer.equals(process.getComputer())) {
					ram = ram + process.ram;
					used = true;
					pcount++;
				}
			}
			if (ram > computer.ram) {
				hard = hard - (ram-computer.ram);
			}
			if (used) {
				soft = soft - computer.cost;
			}
		}
		//System.out.println("Hard: " + hard + ", Soft: " + soft);
		//System.out.println("PC: " + ccount + " Pro: " + pcount);
		return HardSoftScore.valueOf(hard, soft);
	}

}
