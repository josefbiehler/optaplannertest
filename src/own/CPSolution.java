package own;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.value.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.solution.Solution;

@PlanningSolution
public class CPSolution implements Solution<HardSoftScore>{

	List<Computer> computers;
	List<Process> processes;
	HardSoftScore score;
	
	@ValueRangeProvider(id = "computerList")
	public List<Computer> getComputer() {
		return computers;
	}

	@PlanningEntityCollectionProperty
	public List<Process> getProcesses() {
		return processes;
	}
	
	@Override
	public Collection<? extends Object> getProblemFacts() {
		List<Object> facts = new ArrayList<>();
		facts.addAll(computers);
		return facts;
	}

	@Override
	public HardSoftScore getScore() {
		return score;
	}
	
	public CPSolution() {}

	@Override
	public void setScore(HardSoftScore arg0) {
		score = arg0;
	}
	
	public CPSolution(int pc, int pro) {
		computers = new ArrayList<>();
		processes = new ArrayList<>();
		Random random = new Random();
		for(int i = 0; i < pc; i++) {
			Computer computer = new Computer();
			computer.ram = 100000;
			computer.cost = random.nextInt(99999);
			this.computers.add(computer);
		}
		for(int q = 0; q < pro; q++) {
			Process p = new Process();
			p.ram = 10;
			this.processes.add(p);
		}
	}
	
	public String toString() {
		String retVal = "";
		for(Computer c : this.getComputer()) {
			int ram = 0;
			String ramStr = "";
			for(Process p : this.getProcesses()) {
				if (c.equals(p.getComputer())) {
					ram = ram + p.ram;
					ramStr = ramStr + "Process " + p.ownId;
				}
			}
			retVal = retVal + "Computer " + c.ownId + " Ram: " + c.ram + " Used: " + ram + "\r\n----" + ramStr + "\r\n";
		}
		return retVal;
	}
}
