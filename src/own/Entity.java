package own;

public abstract class Entity {
	public abstract void incId();
	public abstract int getId();
	public Entity() {
		incId();
	}
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (! (o instanceof Entity)) {
			return false;
		}
		if (o == this) {
			return true;
		}
		return this.getId() == ((Entity)o).getId();
	}
}
