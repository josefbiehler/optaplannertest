package apt.domain;

public abstract class Domain {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean retVal = false;
		
		if (o == this) {
			return true;
		}
		
		if (o == null) {
			return false;
		}
		
		if (!(o instanceof Domain)) {
			return false;
		}
		
		Domain d = (Domain)o;
		
		if (d.getId() == this.getId()) {
			return true;
		}
		return retVal;
	}
	
	@Override
	public int hashCode() {
		return this.getId();
	}
}
