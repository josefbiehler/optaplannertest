package apt.domain;

import java.util.ArrayList;

public class Period extends Domain{
	private int num;
	private ArrayList<PeriodOrder> periodOrders;

	public Period() {}
	
	public Period(int num) {
		this.num = num;
		periodOrders = new ArrayList<PeriodOrder>();
		setId(num);
	}
	
	public void addPeriodOrder(Order order, int weight) {
		PeriodOrder po = new PeriodOrder(order, this, weight);
		periodOrders.add(po);
	}
	
	public int getWeightForOrder(Order o) {
		for(PeriodOrder po : periodOrders) {
			if (po.getOrder().equals(o)) {
				return po.getWeight();
			}
		}
		// else: neutral
		return 1;
	}
}
