package apt.domain;

public class PeriodOrder extends Domain{
	private Order order;
	private Period period;
	private int weight;
	
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public PeriodOrder(Order order, Period period, int weight) {
		this.order = order;
		this.period = period;
		this.weight = weight;
		///setId()
	}
}
