package apt.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.value.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.solution.Solution;

import apt.data.Import;

@PlanningSolution
public class SolOrders implements Solution<SimpleScore> {

	private List<Order> orders;
	private List<Period> periods;
	private SimpleScore score;
	public int[] lagerzugang;
	public int[] lagerbestand;
	
	//public SolOrders() {}
	public SolOrders() {
		// Data
		/*
		int[] os = {1,1,1,1,1,1,1,1};
		int ps = 8;
		int[] weights = {5,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,10,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,5};
		int[] q1 = {0,1,0,2,0,5,0,0,0};
		int[] q2 = {0,0,0,0,0,0,0,0,0};
		*/
		Import i = new Import();
		try {
			i.a();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int[] os = i.Orders;
		int ps = i.Periods;
		int[] weights = i.Weights;
		int[] q1 = i.Lagerzugang;
		int[] q2 = new int[ps+1];
		Arrays.fill(q2, 0);
		q2[0] = i.L0;
		
		
		lagerzugang = q1;
		lagerbestand = q2;
		
		Map<Order, int[]> orderWeights = GenerateData.generateOrderWeights(os, weights, ps);
		ArrayList<Period> p = GenerateData.generatePeriods(ps, orderWeights);
		periods = p;
		orders = new ArrayList<Order>();
		for(Entry<Order,int[]> entry : orderWeights.entrySet()) {
			orders.add(entry.getKey());
		}
		
	}
	@ValueRangeProvider(id="periods") 
	public List<Period> getPossiblePeriods() {
		return periods;
	}
	
	@PlanningEntityCollectionProperty
	public List<Order> getOrder() {
		return orders;
	}
	
	@Override
	public Collection<? extends Object> getProblemFacts() {
		return orders;
	}

	@Override
	public SimpleScore getScore() {
		return score;
	}

	@Override
	public void setScore(SimpleScore arg0) {
		score = arg0;
	}
	
	@Override
	public String toString() {
		String s = "";
		for(Order o : orders) {
			s = s + "nr: " + o.getId() + "Periode: "  + o.getPeriod().getId() + "\r\n";
		}
		return s;
	}

}
