
package apt.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class Order extends Domain{
	private int volume;
	private int num;
	private Period period;
	
	public Order() {}
	
	public Order(int num, int volume) {
		this.num = num;
		this.volume = volume;
		setId(num);
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"periods"})
	public Period getPeriod() {
		return this.period;
	}
	
	public void setPeriod(Period p) {
		this.period = p;
	}
	
	public int getVolume() {
		return this.volume;
	}
}
