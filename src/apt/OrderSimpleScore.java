package apt;

import java.util.List;

import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.score.director.simple.SimpleScoreCalculator;

import apt.domain.*;

public class OrderSimpleScore implements SimpleScoreCalculator<SolOrders>{

	@Override
	public Score calculateScore(SolOrders arg0) {
		int score = 0;
		List<Period> periods = arg0.getPossiblePeriods();
		List<Order> orders = arg0.getOrder();
		/*int l0 = 0; // Lagerbestand der letzten Periode
		int l1 = 20; // Lagerbestand am Ende der Aktuellen Periode, Anfangs: Anfanglagerbestand
		int[] lagerzugang = {0,20,0,100,0,0};
		int[] lagerbestand = {20,0,0,0,0,0};
		*/
		int[] lagerzugang = arg0.lagerzugang;
		int[] lagerbestand = arg0.lagerbestand;
		
		boolean hard = false;
		// calculate lagerbestand ohne regeln zu beachten (d.h. negative best�nde sind m�glich)
		for(int t = 1; t < periods.size(); t++) {
			int l_last = lagerbestand[t-1];
			//lagerzugang starts with period 1 = index 0
			int l_incoming = lagerzugang[t-1];
			lagerbestand[t] = l_last+l_incoming;
			// t-1 because listcount starts at 0
			Period thatP = periods.get(t-1);
			for(Order o : orders) {
				if (o.getPeriod() != null && o.getPeriod().equals(thatP)) {
					int needed = o.getVolume();
					lagerbestand[t] = lagerbestand[t]-needed;
					if (lagerbestand[t] < 0) {
						hard = true;
						break;
					}
				}
			}

		}
		if (hard == false) {
			// ich muss nicht checken, ob order zweimal gesetzt wird, da dies gar nicht mglich ist
			// je gr��er Z desto schlechter score in qptaplanner! -> berechne Z normal und negiere dann
			//for(int t = 1; t < periods.size(); t++) {
				//calc Z
				int z = 0;
				for(int o = 0; o < orders.size(); o++) {
					for(int t = 0; t < periods.size(); t++) {
						// hole gewichtung f�r diese Zelle
						Period p = periods.get(t);
						Order or = orders.get(o);
						int weight = p.getWeightForOrder(or);
						if (or.getPeriod() != null && or.getPeriod().equals(p)) {
							// nur wenn doe order in der priode bedient wird
							z = z + weight;
						}
					}
				}
				// next summand
				for(int ls : lagerbestand) {
					z = z - ls;
				}
				//z = z * (-1);
				score = z;
			//}
		}else{
			score = Integer.MIN_VALUE;
		}
		return SimpleScore.valueOf(score);
	}

}
