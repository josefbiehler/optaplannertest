package apt.data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class Import {
	public int Periods;
	public int L0;
	public int[] Orders;
	public int[] Lagerzugang;
	public int[] Weights;
	
	public void a() throws Exception {
	    CSVReader reader = new CSVReader(new FileReader("C:/Users/jbiehler/GIT/OptaPlannerTest/src/apt/data/examples/order2.csv"),';');
	    String [] volume = reader.readNext();
	    String [] lageranfangsbestand = reader.readNext();
	    String [] lagerzugang = reader.readNext();
	    String [] perioden = reader.readNext();
	    
	    int[] orders = new int[volume.length-1];
	    int l0 = Integer.parseInt(lageranfangsbestand[1]);
	    int[] lzugang = new int[lagerzugang.length-1];
	    int periods = Integer.parseInt(perioden[1]);
	    
	    for(int i = 0; i < orders.length; i++) {
	    	orders[i] = Integer.parseInt(volume[i+1]);
	    }
	    
	    for(int i = 0; i < lagerzugang.length-1; i++) {
	    	lzugang[i] = Integer.parseInt(lagerzugang[i+1]);
	    }
	    
	    reader = new CSVReader(new FileReader("C:/Users/jbiehler/GIT/OptaPlannerTest/src/apt/data/examples/gewichtung2.csv"),';');
	    reader.readNext();
	    List<Integer[]> listOfPeriods = new ArrayList<Integer[]>();
	    
	    for(int p = 0; p < periods; p++) {
	    	String[] line = reader.readNext();
	    	Integer[] period_p = new Integer[periods];
	    	listOfPeriods.add(period_p);
	    	
	    	for(int vol = 1; vol < line.length; vol++) {
	    		int weight;
	    		try {
	    			weight = Integer.parseInt(line[vol]);
	    		}catch(NumberFormatException e) {
	    			weight = 0;
	    		}
	    		period_p[vol-1]=weight;
	    	}
	    }
	    reader.close();
	    
	    int[] weights = new int[listOfPeriods.size()*(listOfPeriods.get(0)).length];
	    int weightsIndex = 0;
	    
	    int index_o = 0;
	    int index_p = 0;

	    for(int o = 0; o < orders.length; o++) {
	    	for(int p = 0; p < periods; p++) {
	    		weights[weightsIndex] = listOfPeriods.get(p)[o];
	    		weightsIndex++;
	    	}
	    }
	    /*
	    for(Object weightsAr : listOfOrders) {
	    	int[] a = (int[])weightsAr;
    		weights[weightsIndex] = aa;
    		weightsIndex++;
	    }*/
	    
	    Periods = periods;
	    L0 = l0;
	    Lagerzugang = lzugang;
	    Orders = orders;
	    Weights = weights;
	    
	    return;
	}
}
