package apt;

import java.util.ArrayList;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.XmlSolverFactory;

import apt.domain.GenerateData;
import apt.domain.Period;
import apt.domain.SolOrders;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SolverFactory solverFact = new XmlSolverFactory("/apt/config.xml");
		Solver solver = solverFact.buildSolver();
		SolOrders unsolved = new SolOrders();
		solver.setPlanningProblem(unsolved);
		solver.solve();
		SolOrders solved = (SolOrders)solver.getBestSolution();
		System.out.println(solved);
	}

}
