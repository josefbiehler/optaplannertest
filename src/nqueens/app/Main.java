package nqueens.app;

import nqueens.domain.SolQueens;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.XmlSolverFactory;


public class Main {
    public static void main(String[] args) {
    SolverFactory solverFactory = new XmlSolverFactory("/nqueens/app/config.xml");
    Solver solver2 = solverFactory.buildSolver();
    SolQueens unsolved = new SolQueens(100);
    solver2.setPlanningProblem(unsolved);
    solver2.solve(); 
    SolQueens solved = (SolQueens) solver2.getBestSolution();

    //System.out.println(solved.toString());
    }
}
