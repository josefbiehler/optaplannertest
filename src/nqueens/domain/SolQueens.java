package nqueens.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.value.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.solution.Solution;

@PlanningSolution
public class SolQueens implements Solution<SimpleScore>{

	private SimpleScore score;
	private List<Row> rows;
	private List<Queen> queens;
	
	public SolQueens() {}
	public SolQueens(int n) {
		RowChangeMove.n = n;
		rows = new ArrayList<>();
		queens = new ArrayList<>();
		for(int i = 0; i < n; i++) {
			Row row = new Row();
			row.setId(i);
			row.pos = i;
			rows.add(row);
			Queen queen = new Queen();
			Column column = new Column();
			column.setId(i);
			column.pos = i;
			queen.setColumn(column);
			queen.setId(i);
			queens.add(queen);
		}
	}
	@ValueRangeProvider(id="rows")
	public List<Row> getPossibleRows() {
		return rows;
	}
	@PlanningEntityCollectionProperty
	public List<Queen> getQueens() {
		return queens;
	}
	@Override
	public Collection<? extends Object> getProblemFacts() {
		return queens;
	}

	@Override
	public SimpleScore getScore() {
		return score;
	}

	@Override
	public void setScore(SimpleScore arg0) {
		score = arg0;
	}

	@Override
	public String toString() {
		char[][] charAr = new char[rows.size()][rows.size()];
		for(int i = 0; i < rows.size(); i++) {
			for(int q = 0; q < rows.size(); q++) {
				charAr[i][q] = '-';
			}
		}
		for(Queen queen : queens) {
			int col = queen.getColumn().pos;
			int row = queen.getRow().pos;
			charAr[col][row] = 'x';
		}

		String retVal = "";
		for(int i = 0; i < rows.size(); i++) {
			System.out.print("|");
			for(int q = 0; q < rows.size(); q++) {
				System.out.print(charAr[i][q] + "|");
			}
			System.out.println("");
		}
		return retVal;
	}
}
