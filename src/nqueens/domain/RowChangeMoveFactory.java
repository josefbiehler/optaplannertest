package nqueens.domain;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.move.Move;

public class RowChangeMoveFactory implements MoveListFactory<SolQueens> {

	@Override
	public List<? extends Move> createMoveList(SolQueens arg0) {
		List<RowChangeMove> moves = new ArrayList<>();
		List<Queen> queens = arg0.getQueens();
		List<Row> rows = arg0.getPossibleRows();
		int i = 0;
		for(Queen q : queens) {
			for(Row r : rows) {
				RowChangeMove m = new RowChangeMove(r, q);
				m.setId(i);
				i++;
				moves.add(m);
			}
		}
		
		return moves;
	}

}
