package nqueens.domain;

import java.util.List;

import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.score.director.simple.SimpleScoreCalculator;

public class SimpleQueenScore implements SimpleScoreCalculator<SolQueens>{

	@Override
	public Score calculateScore(SolQueens arg0) {
		//simple: only check for vertical match
		int score = 0;
		for(Queen queen : arg0.getQueens()) {
			if (queen.getRow() != null) {
				int row = queen.getRow().pos;
				for(Queen queenCompare : arg0.getQueens()) {
					// do not compare themselfs
					if (!queen.equals(queenCompare)) {
						if (queenCompare.getRow() != null && queenCompare.getRow().pos == row) {
							score = score -1;
							//break;
						}
					}
				}
			}
		}
		
		for(int i = 0; i < arg0.getQueens().size(); i++) {
			int sc = checkVertical(arg0.getPossibleRows(), arg0.getQueens().get(i), arg0.getQueens());
			if (sc != 0) {
				score = score - sc;
				//break;
			}
		}
		return SimpleScore.valueOf(score);
	}

	private int checkVertical(List<Row> rows, Queen curQueen, List<Queen> queens) {
		boolean retVal = false;
		int sc = 0;
		
		//to left
		int q = 1;
		for(int i = curQueen.getColumn().pos-1; i >= 0; i--) {
			if (queens.get(i).getRow() != null && curQueen.getRow() != null &&
					(curQueen.getRow().pos - q == queens.get(i).getRow().pos || 
					curQueen.getRow().pos + q == queens.get(i).getRow().pos)) {
				sc = sc + 1;
			}
			q++;
		}
		
		q = 1;
		for(int i = curQueen.getColumn().pos+1; i < rows.size(); i++) {
			if (queens.get(i).getRow() != null && curQueen.getRow() != null &&
					(curQueen.getRow().pos - q == queens.get(i).getRow().pos || 
					curQueen.getRow().pos + q == queens.get(i).getRow().pos)) {
				sc = sc + 1;
			}
			q++;
		}
		
		return sc;
	}
}
