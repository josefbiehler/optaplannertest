package nqueens.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.optaplanner.core.impl.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;

public class RowChangeMove extends Domain implements Move {

	Row toRow;
	Queen queen;
	public static int n;
	public static int nextId;
	
	@Override
	public void setId(int id) {
		super.setId(id);
		nextId = this.getId() + 1;
	}
	public RowChangeMove(Row r, Queen q) {
		toRow = r;
		queen = q;
	}
	
	@Override
	public Move createUndoMove(ScoreDirector arg0) {
		RowChangeMove m = new RowChangeMove(queen.getRow(), queen);
		m.setId(nextId);
		return m;
	}

	@Override
	public void doMove(ScoreDirector arg0) {
		arg0.beforeVariableChanged(queen, "row");
		queen.setRow(toRow);
		arg0.afterVariableChanged(queen, "row");
	}

	@Override
	public Collection<? extends Object> getPlanningEntities() {
		return Collections.singletonList(queen);
	}

	@Override
	public Collection<? extends Object> getPlanningValues() {
		return Collections.singletonList(toRow);
	}

	@Override
	public boolean isMoveDoable(ScoreDirector arg0) {
		if (queen.getRow() != null && queen.getRow().equals(toRow)) {
			return false;
		}
		List<Queen> queens = ((SolQueens)arg0.getWorkingSolution()).getQueens();
		List<Row> rows = ((SolQueens)arg0.getWorkingSolution()).getPossibleRows();
		int leftPos = -1;
		int rightPos = -1;
		int thisPos = this.toRow.pos;
		int thisColumn = queen.getColumn().pos;
		// get left
		if (thisColumn != 0) {
			Queen leftColumn = queens.get(thisColumn-1);
			leftPos = leftColumn.getRow().pos;
		}
		// get right
		if (thisColumn != rows.size()-1) {
			Queen rightColumn = queens.get(thisColumn+1);
			rightPos = rightColumn.getRow().pos;
		}
		if (leftPos == thisPos || leftPos == thisPos+1 || leftPos == thisPos-1 || rightPos == thisPos || rightPos == thisPos+1 || rightPos == thisPos-1) {
			return false;
		}
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (o instanceof RowChangeMove) {
			RowChangeMove row = (RowChangeMove)o;
			if (row.queen.equals(this.queen) && row.toRow.equals(this.toRow)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.getId();
	}
}
