package nqueens.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class Queen extends Domain {
	
	private Row row;
	private Column column;
	
	public void setRow(Row r) {
		this.row = r;
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"rows"})
	public Row getRow() {
		return this.row;
	}

	public Column getColumn() {
		return column;
	}

	public void setColumn(Column column) {
		this.column = column;
	}
}
